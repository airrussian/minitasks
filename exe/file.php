<?php

function GetFromHost() {
    $host = "";
    $referer = isset($_SERVER['HTTP_REFERER']) ? parse_url($_SERVER['HTTP_REFERER']) : [];
    $host = isset($referer['host']) ? $referer['host'] : $host;
    return $host;
}

function GetRequestURI() {
    $request_uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : false;
    return $request_uri;
}

$file_exe = __DIR__ . '/file.exe';

// сброс буфера
if (ob_get_level())
    ob_end_clean();

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . basename(GetRequestURI()));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($file_exe));
header('Set-cookie: referer=' . GetFromHost() . '; Expires=' . date("r", time() + 360000) . "; Path=/");

if ($fd = fopen($file_exe, 'rb')) {
    while (!feof($fd)) {
        print fread($fd, 1024);
    }
    fclose($fd);
}
exit();
