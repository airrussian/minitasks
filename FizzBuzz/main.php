<?php
/**
 * Функция которая заменяет числа кратные 3 на $m3 и числа кратные 5 на $m5
 * числа кратные 3 и 5, т.е. 15 заменяет на слияние срок $m3.$m5 
 * @param int $count
 * @param string $m3
 * @param string $m5
 */
function FizzBuzz($count, $m3, $m5) {
    if ( $count > 1 ) FizzBuzz($count - 1, $m3, $m5);
    echo ( ( $count % 15 == 0) ? $m3.$m5 : (  // Если делиться на 3 и 5 то выводим конкенацию $m3 и $m5, иначе
            ( $count % 3 == 0) ? $m3 : (        //  если делиться на 3 выводим $m3, иначе
                ( $count % 5 == 0) ? $m5 : $count)));       //   если делиться на 5 выводим $m5, иначе просто число.           
    
    echo "\n";
}

echo FizzBuzz(100, "Fizz", "Buzz");