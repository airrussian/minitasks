<?php
// Просто для теста, через console кушаем строку (строка должна быть в кавычках)
$string = $argv[1]; 

// Вот и всё тут будет ответ по условию теста. 
echo polyDrom($string); 

/**
 * Сравнивает две строки $string1, $string2, путем удаление из каждой
 * пробелов и приведение их в нижний регистр 
 * @param string $string1
 * @param string $string2
 * @return boolean
 */
function compare($string1, $string2) {    
    $string1 = strtolower(str_replace(" ", "", $string1));
    $string2 = strtolower(str_replace(" ", "", $string2));    
    return ($string1 == $string2);
}

/**
 * Ищет в указанной строке $string максимальный полиндром,
 * если полиндром не найден будет возвращен первый символ строки
 * @param string $string
 * @return string
 */
function polyDrom($string) {    
    // проверяем равны ли оригинал и ревернутая строка    
    // Инвариант выход из рекурсии при строке в длину 1 
    // она не избежно будет равна себе. 
    if (compare($string, strrev($string))) return $string;
    
    $rRes = polyDrom(substr($string, -1 * (strlen($string) - 1)));    
    $lRes = polyDrom(substr($string, 0, strlen($string) - 1));
    return (strlen($lRes) < strlen($rRes)) ? $rRes : $lRes;
}
